#pragma once

#include <vector>
#include <math.h>
//#include "drlgl/globject.h"

#include "vector2d.h"

#include "obstacle.h"


class Ellipse  : public Obstacle2D
{
public:
    Ellipse(double ix=0, double iy=0, double radX=0.4, double radY=0.4, double yawAngle=0.0){center.x=ix; center.y=iy; radius.x=radX; radius.y=radY; this->yawAngle=yawAngle; type = ELLIPSE;}
    virtual ~Ellipse(){}

	Vector2D center;
    Vector2D radius;
    double yawAngle;
	
};




